window.onload=function(){  

    var selector_valoracion=document.getElementById('Valoracion');
    console.log(selector_valoracion);
    console.log("El dato 1");

    document.getElementById('Frecuencias').disabled = true;  

    var selector_frecuencia=document.getElementById('Frecuencias');
    selector_valoracion.addEventListener("change", function(){
        if (selector_valoracion.value == 'Valoración tradicional' || selector_valoracion.value == "Valoración de duración") {
            selector_frecuencia.disabled = false;
            verificarSeleccion();
        }else {
            selector_frecuencia.disabled = true;
        }
    });
}

function verificarSeleccion(){
    console.log("entro a verif select");
    var selector_frecuencia = document.getElementById('Frecuencias');
    selector_frecuencia.addEventListener("change", function(){
    if (selector_frecuencia.value == "Método 1" || selector_frecuencia.value == "Método 2" || selector_frecuencia.value == "Método 3") {
        mostrarTablas();
        return;
    }else{
    }   
    });
}

function mostrarTablas(){     

        var containerImpacto = document.getElementById('TablaImpacto'),
        tablaImp = new Handsontable(containerImpacto, {
        data: DatosTabla(),
        licenseKey: 'non-commercial-and-evaluation',
        colWidths: 70,
        rowHeights: 30,
        cells: function(row, col, prop) {
          var cellProperties = {};
          if (col == 0 || row == 0 ) {
            cellProperties.readOnly = 'true'
          }
          return cellProperties
        }
      });
         
        var containerDuracion = document.getElementById('TablaDuracion'),
        tablaDur = new Handsontable(containerDuracion, {
        data: DatosTabla(),
        licenseKey: 'non-commercial-and-evaluation',
        colWidths: 70,
        rowHeights: 30,
        cells: function(row, col, prop) {
            var cellProperties = {};
            if (col == 0 || row == 0 ) {
            cellProperties.readOnly = 'true'
            }
            return cellProperties
        }
        });
    
}

function DatosTabla() {
    return [
      { Rango: 'Rango',
        Inf: 'Límite inferior',
        Sup: 'Límite superior',
        Prob: 'Prob %'},
      { Rango: 1,
        Inf: '',
        Sup: '',
        Prob: ''},
      { Rango: 2,
        Inf: '',
        Sup: '',
        Prob: ''},
      { Rango: 3,
        Inf: '',
        Sup: '',
        Prob: ''},
      { Rango: 4,
        Inf: '',
        Sup: '',
        Prob: ''},
      { Rango: 5,
        Inf: '',
        Sup: '',
        Prob: ''},
      { Rango: 6,
        Inf: '',
        Sup: '',
        Prob: ''},
      { Rango: 7,
        Inf: '',
        Sup: '',
        Prob: ''}
    ];
  }



